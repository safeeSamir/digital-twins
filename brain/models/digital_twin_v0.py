from typing import List, Tuple
from transformers import pipeline, Conversation, TFAutoModelForQuestionAnswering, AutoTokenizer, TFBlenderbotForConditionalGeneration, BlenderbotTokenizer
from data.storage import Storage
import os
import time


class DigitalTwin_V0:
    found = False
    if os.path.isdir("./data/saved_models"):
        found = True
        qna_model_path = "./data/saved_models/qna_model"
        conversational_model_path = "./data/saved_models/conversational_model"
    else:
        qna_model_path = "deepset/roberta-base-squad2"
        conversational_model_path = "facebook/blenderbot-400M-distill"

    qna_model = TFAutoModelForQuestionAnswering.from_pretrained(qna_model_path)
    qna_tokenizer = AutoTokenizer.from_pretrained(qna_model_path)
    conversational_model = TFBlenderbotForConditionalGeneration.from_pretrained(
        conversational_model_path)
    conversational_tokenizer = BlenderbotTokenizer.from_pretrained(
        conversational_model_path)
    if not found:
        # saving the models
        conversational_model_path = "./data/saved_models/conversational_model"
        conversational_model.save_pretrained(conversational_model_path)
        conversational_tokenizer.save_pretrained(conversational_model_path)
        qna_model_path = "./data/saved_models/qna_model"
        qna_model.save_pretrained(qna_model_path)
        qna_tokenizer.save_pretrained(qna_model_path)

    def __init__(self, qna_model=qna_model, qna_tokenizer=qna_tokenizer,
                 conversational_model=conversational_model, conversational_tokenizer=conversational_tokenizer) -> None:
        self.qna_pipeline = pipeline(
            'question-answering', model=qna_model, tokenizer=qna_tokenizer)
        self.conversational_pipeline = pipeline(
            'conversational', model=conversational_model, tokenizer=conversational_tokenizer)
        self.ongoing_conversations: List[Conversation] = []
        self.storage = Storage()

    def build_convo(self, messages):
        conversation = Conversation()
        if messages == None:
            return conversation
        for message in messages:
            if message['isBot']:
                conversation.append_response(message['message'])
            else:
                conversation.add_user_input(message['message'])
        conversation.mark_processed()
        return conversation

    def get_conversation(self, conversation_id):
        for convo in self.ongoing_conversations:
            if str(convo.uuid) == conversation_id:
                return convo

    def get_response(self, target_avatar_name, conversation_id, message):
        timeStamp = time.time()
        context = self.storage.retrieve_context(target_avatar_name)
        if context == None:
            context = " "
        response = self.qna_pipeline(question=message, context=context)
        if (response['score'] > 0.01 and response['start'] != 0) or response['score'] > 0.1:
            print(response)
            convo = self.get_conversation(conversation_id)
            if convo == None:
                messages = self.storage.retrieve_messages(conversation_id)
                convo = self.build_convo(messages)
            convo.add_user_input(message)
            convo.append_response(response['answer'])
            convo.mark_processed()
            avatarTimeStamp = time.time()
            response = {'message': response['answer'],
                        'conversationID': str(convo.uuid),
                        'timeStamp': timeStamp}
        else:
            convo = self.get_conversation(conversation_id)
            if convo != None:
                convo.add_user_input(message)
                response = self.conversational_pipeline(convo)
            else:
                messages = self.storage.retrieve_messages(conversation_id)
                convo = self.build_convo(messages)
                convo.add_user_input(message)
                response = self.conversational_pipeline(convo)
                self.ongoing_conversations.append(response)
            avatarTimeStamp = time.time()
            response = {'message': convo.generated_responses[len(convo.generated_responses)-1],  # last generated message
                        'conversationID': str(convo.uuid),
                        'timeStamp': avatarTimeStamp}

        self.storage.store_message(
            message, response['conversationID'], timeStamp, False)
        self.storage.store_message(
            response['message'], response['conversationID'], avatarTimeStamp, True)
        return response

    def add_context(self, avatar_name, context):
        self.storage.add_context(avatar_name, context)
