from flask import Flask, jsonify, request, Response
from models import DigitalTwin_V0
app = Flask(__name__)

model = DigitalTwin_V0()


@app.route("/")
def hello_world():
    return "<h1>Hello, World!</h1>"


@app.route('/talk/', methods=['POST', 'GET'])
def talk():
    avatar_name = request.args.get('avatar_name', None)
    conversation_id = request.args.get('conversation_id', None)
    message = request.args.get('message', None)
    if avatar_name is None:
        avatar_name = " "
    if conversation_id is None:
        conversation_id = " "
    if message is None:
        message = " "
    return jsonify(model.get_response(avatar_name, conversation_id, message))


@app.route('/add_context/', methods=['POST', 'GET'])
def add_context():
    context = request.args.get('context', None)
    avatar_name = request.args.get('avatar_name', None)
    if context is None or avatar_name is None:
        return Response(status=403)
    else:
        model.add_context(avatar_name, context)
        return Response(status=200)


@app.route('/get_messages/', methods=['POST', 'GET'])
def get_messages():
    conversation_id = request.args.get('conversation_id', None)
    if conversation_id is None:
        return Response(status=403)
    else:
        print(conversation_id)
        messages = model.storage.retrieve_messages(conversation_id)
        if messages == None:
            return Response(status=404)
        else:
            return jsonify(messages)


if __name__ == "__main__":
    app.run(debug=True, port=8000)
