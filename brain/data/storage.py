from threading import Lock
import sqlite3
import contextlib


class SingletonMeta(type):
    """
    This is a thread-safe implementation of Singleton.
    """
    _instances = {}
    _lock: Lock = Lock()

    def __call__(cls, *args, **kwargs):
        with cls._lock:
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
        return cls._instances[cls]


class Storage(metaclass=SingletonMeta):
    def __init__(self) -> None:
        self.db_path = "./data/database.db"
        self.lock = Lock()

    def safe_query(self, query: str, values: dict):
        data = None
        self.lock.acquire()
        with contextlib.closing(sqlite3.connect(self.db_path)) as conn:  # auto-closes
            with conn:  # auto-commits
                with contextlib.closing(conn.cursor()) as cursor:  # auto-closes
                    cursor.execute(query, values)
                    data = cursor.fetchall()
        self.lock.release()
        return data

    def add_context(self, avatar_name: str, context):
        old_context = self.retrieve_context(avatar_name)
        if old_context is None:
            self.safe_query("INSERT INTO avatar VALUES (:name, :context)",
                            {'name': avatar_name,
                             'context': context, })
        else:
            self.safe_query("UPDATE avatar SET context=:context WHERE name=:name ",
                            {'name': avatar_name, 'context': f"{old_context} {context}"})

    def retrieve_context(self, avatar_name):
        context = self.safe_query("SELECT context FROM avatar WHERE name=:name",
                                  {'name': avatar_name})
        if context != []:
            return context[0][0]
        else:
            return None

    def store_message(self, message: str, conversationID, timeStamp: float, is_bot: bool):
        self.safe_query("INSERT INTO conversation VALUES (:conversationID, :message, :timeStamp, :isBot)",
                        {'conversationID': conversationID,
                         'message': message,
                         'timeStamp': timeStamp,
                         'isBot': int(is_bot)
                         })

    def retrieve_messages(self, conversationID):
        messages = self.safe_query("SELECT message, timeStamp, isBot FROM conversation WHERE conversationID=:conversationID",
                                   {'conversationID': conversationID})
        print(messages)
        return [{'message': message, 'timeStamp': timeStamp, 'isBot': bool(isBot)} for message, timeStamp, isBot in messages] if messages != [] else None
