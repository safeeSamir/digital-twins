import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';

export default function ImgMediaCard({imgSrc, avatarName, about}) {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardMedia
        component="img"
        alt="Avatar Image"
        height="140"
        image={imgSrc}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {avatarName}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {about}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">Chat</Button>
        <Button size="small">Add to Favorites</Button>
      </CardActions>
    </Card>
  );
}
