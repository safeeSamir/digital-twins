import { FadingCircle } from 'better-react-spinkit'
import Image from 'next/image'
function Loading() {
  return (
    <center style={{
      display: "grid",
      placeItems: "center",
      height: "100vh"
    }}>
        <div>
            <Image
            src="/dept-logo.png"
            alt="Loading..."
            height={230}
            width={230}
            
            />
            <FadingCircle size={60} color="#515a61"/>
        </div>
    </center>
  )
}

export default Loading