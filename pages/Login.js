import { Button } from '@mui/material'
import Head from 'next/head'
import styled from 'styled-components'
import { auth, provider } from '../firebase'
import { signInWithPopup } from 'firebase/auth'
import { Google } from '@mui/icons-material'
function Login() {
    const signIn = () => {
        signInWithPopup(auth, provider).catch(alert)
    }
  return (
    <Container>
        <Head>
            <title>Login</title>
        </Head>
        <LogoContainer>
            <Logo src='/dept-logo.png' />
            <SignInButton variant='contained' onClick={signIn}>
                <Google 
                    style={{
                        margin: '5px',
                        width: '30px', 
                        height: '30px',
                        marginTop: '-5px',
                        marginLeft: '-5px'

                    }} />
                <SignInButtonText>Sign in with Google</SignInButtonText> 
            </SignInButton>
        </LogoContainer>
    </Container>
  )
}

export default Login

const Container = styled.div`
    display: grid;
    place-items: center;
    height: 100vh;
    background-color: whitesmoke;
`;
const Logo = styled.img`
width: 200px;
height: 200px;
margin-bottom: 50px;
`;
const LogoContainer = styled.div`
    padding: 100px;
    display: flex;
    flex-direction: column;
    align-items: center;
    background-color: white;
    border-radius: 5px;
    box-shadow: 0px 4px 14px -3px rgba(0, 0, 0, 0.7);
`;

const SignInButton = styled(Button)`
   &&&{
    padding-top: 10px;
   } 

`;
const SignInButtonText = styled.span`
    font-size: 1.2em;
    font-weight: bold;
    align-self: center;
`;