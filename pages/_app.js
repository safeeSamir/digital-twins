import '../styles/globals.css'
import { useAuthState } from 'react-firebase-hooks/auth'
import { auth, db } from '../firebase'
import  Login  from './Login'
import Loading from '../components/Loading';
import { useEffect } from 'react';
import { getFirestore, doc, collection, serverTimestamp, setDoc, addDoc } from 'firebase/firestore';
function MyApp({ Component, pageProps }) {
  const [user, loading] = useAuthState(auth);
  useEffect(()=>{
    if(user)
    try {
      const userRef = doc(db, "users", user.uid);
      setDoc(userRef, {
        name: user.displayName,
        email: user.email,
        photoURL: user.photoURL,
        lastSeen: serverTimestamp()
      }, { merge: true });
      console.log("Document written with ID: ", user.uid);
    } catch (e) {
      console.error("Error adding document: ", e);
    }
  }, [user])
  if(loading) return <Loading/>
  if (!user) {
    return <Login/>}

  else return <Component {...pageProps} />
}

export default MyApp
