# Digital Twins

A virtual space with your virtual twin. Your twin can interact with other twins and other people.

## Installation

**Step 1:** Clone the [repository](https://gitlab.com/safeeSamir/digital-twins) and change directory to it.

```bash
cd digital-twins
```

**Step 2:** Create a virtual environment (optional).

```bash
python -m venv digital-twin-env
```

**Step 3:** Activate the virtual environment.

**Linux**

```bash
source digital-twin-env/bin/activate
```

**Windows:**

```
.\digital-twin-env\Scripts\activate
```

**Step 4:** Install packages with pip.

```bash
python -m pip install --upgrade pip
```

```bash
python -m pip install -r requirements.txt
```

**Step 5 (optional):** Add the virtual environment to jupyter notebook. Run these commands when your virtual environment is activated.

```bash
pip install ipykernel
```

```bash
python -m ipykernel install --user --name=digital-twin-env
```

## API Usage

```bash
localhost:8000/get_messages/?conversation_id={conversation_id}
```

```bash
localhost:8000/talk/?message={message}&avatar_name={avatar_name}&conversation_id={conversation_id}
```

```bash
localhost:8000/add_context/?avatar_name={avatar_name}&context={context}
```

## Testing

```bash
python -m ipykernel install --user --name=digital-twin-env
```

Change the python kernel in the testing jupyter notebook to the digital-twin-env or any other name that you used to create your envirorment.
